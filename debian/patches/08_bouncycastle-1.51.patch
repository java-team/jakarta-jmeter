From: Markus Koschany <apo@debian.org>
Date: Sun, 29 Nov 2015 18:42:07 +0100
Subject: bouncycastle 1.51

Fix FTBFS with bouncycastle >= 1.51. This is apparently fixed in the
development version of jmeter. The patch can be dropped with jmeter 2.14.

Forwarded: https://bz.apache.org/bugzilla/attachment.cgi?id=33178
---
 .../apache/jmeter/assertions/SMIMEAssertion.java   | 22 +++++++++++++---------
 .../assertions/SMIMEAssertionTestElement.java      |  5 +++++
 2 files changed, 18 insertions(+), 9 deletions(-)

diff --git a/src/components/org/apache/jmeter/assertions/SMIMEAssertion.java b/src/components/org/apache/jmeter/assertions/SMIMEAssertion.java
index b0baca9..abcb169 100644
--- a/src/components/org/apache/jmeter/assertions/SMIMEAssertion.java
+++ b/src/components/org/apache/jmeter/assertions/SMIMEAssertion.java
@@ -27,12 +27,12 @@ import java.io.InputStream;
 import java.math.BigInteger;
 import java.security.GeneralSecurityException;
 import java.security.Security;
-import java.security.cert.CertStore;
 import java.security.cert.CertificateException;
 import java.security.cert.CertificateFactory;
 import java.security.cert.X509Certificate;
 import java.util.ArrayList;
 import java.util.Iterator;
+import java.util.Collection;
 import java.util.List;
 import java.util.Properties;
 
@@ -53,6 +53,8 @@ import org.bouncycastle.cms.CMSException;
 import org.bouncycastle.cms.SignerInformation;
 import org.bouncycastle.cms.SignerInformationStore;
 import org.bouncycastle.cms.jcajce.JcaX509CertSelectorConverter;
+import org.bouncycastle.cms.jcajce.JcaSimpleSignerInfoVerifierBuilder;
+import org.bouncycastle.operator.OperatorCreationException;
 import org.bouncycastle.jce.PrincipalUtil;
 import org.bouncycastle.jce.X509Principal;
 import org.bouncycastle.jce.provider.BouncyCastleProvider;
@@ -60,6 +62,7 @@ import org.bouncycastle.mail.smime.SMIMEException;
 import org.bouncycastle.mail.smime.SMIMESignedParser;
 import org.bouncycastle.operator.bc.BcDigestCalculatorProvider;
 import org.bouncycastle.x509.extension.X509ExtensionUtil;
+import org.bouncycastle.util.Store;
 
 /**
  * Helper class which isolates the BouncyCastle code.
@@ -73,7 +76,8 @@ class SMIMEAssertion {
         super();
     }
 
-    public static AssertionResult getResult(SMIMEAssertionTestElement testElement, SampleResult response, String name) {
+    public static AssertionResult getResult(SMIMEAssertionTestElement testElement, SampleResult response, String name)
+            throws OperatorCreationException {
         checkForBouncycastle();
         AssertionResult res = new AssertionResult(name);
         try {
@@ -143,19 +147,19 @@ class SMIMEAssertion {
     }
 
     private static AssertionResult verifySignature(SMIMEAssertionTestElement testElement, SMIMESignedParser s, String name)
-            throws CMSException {
+            throws CMSException, OperatorCreationException {
         AssertionResult res = new AssertionResult(name);
 
         try {
-            CertStore certs = s.getCertificatesAndCRLs("Collection", "BC"); // $NON-NLS-1$  // $NON-NLS-2$
+            Store certStore = s.getCertificates(); // $NON-NLS-1$  // $NON-NLS-2$
             SignerInformationStore signers = s.getSignerInfos();
             Iterator<?> signerIt = signers.getSigners().iterator();
 
             if (signerIt.hasNext()) {
 
                 SignerInformation signer = (SignerInformation) signerIt.next();
-                Iterator<?> certIt = certs.getCertificates(
-                        (new JcaX509CertSelectorConverter()).getCertSelector(signer.getSID())).iterator();
+                Collection certCollection = certStore.getMatches(signer.getSID());
+                Iterator<?> certIt = certCollection.iterator();
 
                 if (certIt.hasNext()) {
                     // the signer certificate
@@ -163,9 +167,9 @@ class SMIMEAssertion {
 
                     if (testElement.isVerifySignature()) {
 
-                        if (!signer.verify(cert.getPublicKey(), "BC")) { // $NON-NLS-1$
-                            res.setFailure(true);
-                            res.setFailureMessage("Signature is invalid");
+                        if (!signer.verify(new JcaSimpleSignerInfoVerifierBuilder().setProvider("BC").build(cert))) { // $NON-NLS-1$
+                                res.setFailure(true);
+                                res.setFailureMessage("Signature is invalid");
                         }
                     }
 
diff --git a/src/components/org/apache/jmeter/assertions/SMIMEAssertionTestElement.java b/src/components/org/apache/jmeter/assertions/SMIMEAssertionTestElement.java
index 9fbdd6f..4c9aea4 100644
--- a/src/components/org/apache/jmeter/assertions/SMIMEAssertionTestElement.java
+++ b/src/components/org/apache/jmeter/assertions/SMIMEAssertionTestElement.java
@@ -23,6 +23,7 @@ import java.io.Serializable;
 import org.apache.jmeter.samplers.SampleResult;
 import org.apache.jmeter.testelement.AbstractTestElement;
 import org.apache.jmeter.util.JMeterUtils;
+import org.bouncycastle.operator.OperatorCreationException;
 
 public class SMIMEAssertionTestElement extends AbstractTestElement implements
         Serializable, Assertion {
@@ -57,6 +58,10 @@ public class SMIMEAssertionTestElement extends AbstractTestElement implements
             assertionResult.setResultForFailure(JMeterUtils
                 .getResString("bouncy_castle_unavailable_message")); //$NON-NLS-1$
             return assertionResult;
+        } catch (OperatorCreationException e) {
+            AssertionResult assertionResult = new AssertionResult(getName());
+            assertionResult.setError(true);
+            return assertionResult;
         }
     }
 
